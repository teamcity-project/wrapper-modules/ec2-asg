variable "region" {
  description = "AWS region"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
  default     = ""
}

variable "asg_name" {
  description = "Auto scaling group name"
  type        = string
}

variable "e2_ami_filter" {
  description = "Filter for AWS AMI"
  type        = list(string)
  default     = ["amzn-ami-hvm-*-x86_64-gp2"]
}

variable "vpc_security_group_ids" {
  description = "List of VPC security groups to associate with EC2"
  type        = list(string)
  default     = []
}

variable "ssh_vpc_id" {
  description = "The VPC for SSH SG creating"
  type        = string
}

variable "subnet_ids" {
  description = "A list of VPC subnet IDs for EC2"
  type        = list(string)
}

variable "ec2_instance_type" {
  description = "ECS EC2 instance type"
  type        = string
}

variable "spot_price" {
  description = "The price to use for reserving spot instances"
  type        = string
  default     = ""
}

variable "ssh_key_pair_name" {
  description = "The SSH key name attached to ECS EC2"
  type        = string
  default     = ""
}

variable "asg_min_size" {
  description = "The minimum size of the auto scale group of EC2 instances"
  type        = number
  default     = 1
}

variable "asg_max_size" {
  description = "The maximum size of the auto scale group of EC2 instances"
  type        = number
  default     = 5
}

variable "asg_desired_capacity" {
  description = "Desired number of ECS EC2 instances in auto scale group"
  type        = number
  default     = 1
}

variable "recreate_asg_when_lc_changes" {
  description = "Whether to recreate an autoscaling group when launch configuration changes"
  type        = bool
  default     = false
}

variable "associate_public_ip_address" {
  description = "Associate a public ip address with an instance in a VPC. Use if the VPC is private only"
  type        = bool
  default     = false
}

variable "root_block_device" {
  description = "Customize details about the root block device of the instance"
  type        = list(map(string))
  default     = []
}

variable "ebs_block_device" {
  description = "Additional EBS block devices to attach to the instance"
  type        = list(map(string))
  default     = []
}

variable "ephemeral_block_device" {
  description = "Customize Ephemeral (also known as 'Instance Store') volumes on the instance"
  type        = list(map(string))
  default     = []
}

variable "ssh_whitelist_cidr" {
  description = "Source CIDR list allowed for SSH connect"
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
