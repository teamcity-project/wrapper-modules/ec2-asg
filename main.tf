terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  required_providers {
    aws = "~> 2.20"
  }
}

provider "aws" {
  region = var.region
}

data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = var.e2_ami_filter
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

module "ec2_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "${var.asg_name}"

  # Launch configuration
  lc_name                     = "${var.asg_name}-lc"
  image_id                    = data.aws_ami.amazon_linux.id
  spot_price                  = var.spot_price
  instance_type               = var.ec2_instance_type
  key_name                    = var.ssh_key_pair_name
  associate_public_ip_address = var.associate_public_ip_address
  security_groups = concat(
    var.vpc_security_group_ids,
    list(module.sg_ssh.this_security_group_id)
  )

  root_block_device      = var.root_block_device
  ebs_block_device       = var.ebs_block_device
  ephemeral_block_device = var.ephemeral_block_device

  # Auto scaling group
  asg_name                     = "${var.asg_name}-asg"
  vpc_zone_identifier          = var.subnet_ids
  health_check_type            = "EC2"
  min_size                     = var.asg_min_size
  max_size                     = var.asg_max_size
  desired_capacity             = var.asg_desired_capacity
  recreate_asg_when_lc_changes = var.recreate_asg_when_lc_changes

  # TODO: add suffix
  tags_as_map = merge(var.tags, map("ASG", format("%s", "${var.asg_name}-asg")))
}

module "sg_ssh" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name                = "${var.asg_name}-ext-ssh-sg"
  description         = "External access via SSH"
  vpc_id              = var.ssh_vpc_id
  ingress_cidr_blocks = var.ssh_whitelist_cidr
  ingress_rules       = ["ssh-tcp"]

  tags = var.tags
}
