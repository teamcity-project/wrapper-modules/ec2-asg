output "ssh_security_group_id" {
  value       = module.sg_ssh.this_security_group_id
  description = "SSH Security Group ID"
}

output "ssh_security_group_name" {
  value       = module.sg_ssh.this_security_group_name
  description = "SSH Security Group name"
}
