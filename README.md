# ALB wrapper module for Teamcity project

This module is wrapper for [terraform-aws-security-group](https://github.com/terraform-aws-modules/terraform-aws-security-group)
and [terraform-aws-autoscaling](https://github.com/terraform-aws-modules/terraform-aws-autoscaling) modules.

It used for creating an ASG for bastion host, not for ASG ECS.

The module creates:
  - Launch configuration
  - Auto scaling group
  - Security group for external access to bastion host

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.20 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| asg\_desired\_capacity | Desired number of ECS EC2 instances in auto scale group | `number` | `1` | no |
| asg\_max\_size | The maximum size of the auto scale group of EC2 instances | `number` | `5` | no |
| asg\_min\_size | The minimum size of the auto scale group of EC2 instances | `number` | `1` | no |
| asg\_name | Auto scaling group name | `string` | n/a | yes |
| associate\_public\_ip\_address | Associate a public ip address with an instance in a VPC. Use if the VPC is private only | `bool` | `false` | no |
| e2\_ami\_filter | Filter for AWS AMI | `list(string)` | <pre>[<br>  "amzn-ami-hvm-*-x86_64-gp2"<br>]</pre> | no |
| ebs\_block\_device | Additional EBS block devices to attach to the instance | `list(map(string))` | `[]` | no |
| ec2\_instance\_type | ECS EC2 instance type | `string` | n/a | yes |
| environment | Environment name | `string` | `""` | no |
| ephemeral\_block\_device | Customize Ephemeral (also known as 'Instance Store') volumes on the instance | `list(map(string))` | `[]` | no |
| recreate\_asg\_when\_lc\_changes | Whether to recreate an autoscaling group when launch configuration changes | `bool` | `false` | no |
| region | AWS region | `string` | n/a | yes |
| root\_block\_device | Customize details about the root block device of the instance | `list(map(string))` | `[]` | no |
| spot\_price | The price to use for reserving spot instances | `string` | `""` | no |
| ssh\_key\_pair\_name | The SSH key name attached to ECS EC2 | `string` | `""` | no |
| ssh\_vpc\_id | The VPC for SSH SG creating | `string` | n/a | yes |
| ssh\_whitelist\_cidr | Source CIDR list allowed for SSH connect | `list(string)` | `[]` | no |
| subnet\_ids | A list of VPC subnet IDs for EC2 | `list(string)` | n/a | yes |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |
| vpc\_security\_group\_ids | List of VPC security groups to associate with EC2 | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| ssh\_security\_group\_id | SSH Security Group ID |
| ssh\_security\_group\_name | SSH Security Group name |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## To Do
* Use the module for implementing an ECS ASG
